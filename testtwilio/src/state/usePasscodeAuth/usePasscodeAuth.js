"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getErrorMessage = exports.verifyPasscode = exports.fetchToken = exports.getPasscode = void 0;
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var endpoint = process.env.REACT_APP_TOKEN_ENDPOINT || '/token';
function getPasscode() {
    var match = window.location.search.match(/passcode=(.*)&?/);
    var passcode = match ? match[1] : window.sessionStorage.getItem('passcode');
    return passcode;
}
exports.getPasscode = getPasscode;
function fetchToken(name, room, passcode, create_room, create_conversation) {
    if (create_room === void 0) { create_room = true; }
    if (create_conversation === void 0) { create_conversation = process.env.REACT_APP_DISABLE_TWILIO_CONVERSATIONS !== 'true'; }
    return fetch(endpoint, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify({
            user_identity: name,
            room_name: room,
            passcode: passcode,
            create_room: create_room,
            create_conversation: create_conversation,
        }),
    });
}
exports.fetchToken = fetchToken;
function verifyPasscode(passcode) {
    var _this = this;
    return fetchToken('temp-name', 'temp-room', passcode, false /* create_room */, false /* create_conversation */).then(function (res) { return __awaiter(_this, void 0, void 0, function () {
        var jsonResponse;
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, res.json()];
                case 1:
                    jsonResponse = _b.sent();
                    if (res.status === 401) {
                        return [2 /*return*/, { isValid: false, error: (_a = jsonResponse.error) === null || _a === void 0 ? void 0 : _a.message }];
                    }
                    if (res.ok && jsonResponse.token) {
                        return [2 /*return*/, { isValid: true }];
                    }
                    return [2 /*return*/];
            }
        });
    }); });
}
exports.verifyPasscode = verifyPasscode;
function getErrorMessage(message) {
    switch (message) {
        case 'passcode incorrect':
            return 'Passcode is incorrect';
        case 'passcode expired':
            return 'Passcode has expired';
        default:
            return message;
    }
}
exports.getErrorMessage = getErrorMessage;
function usePasscodeAuth() {
    var _this = this;
    var history = (0, react_router_dom_1.useHistory)();
    var _a = (0, react_1.useState)(null), user = _a[0], setUser = _a[1];
    var _b = (0, react_1.useState)(false), isAuthReady = _b[0], setIsAuthReady = _b[1];
    var getToken = (0, react_1.useCallback)(function (name, room) {
        return fetchToken(name, room, user.passcode)
            .then(function (res) { return __awaiter(_this, void 0, void 0, function () {
            var json, errorMessage;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (res.ok) {
                            return [2 /*return*/, res];
                        }
                        return [4 /*yield*/, res.json()];
                    case 1:
                        json = _b.sent();
                        errorMessage = getErrorMessage(((_a = json.error) === null || _a === void 0 ? void 0 : _a.message) || res.statusText);
                        throw Error(errorMessage);
                }
            });
        }); })
            .then(function (res) { return res.json(); });
    }, [user]);
    var updateRecordingRules = (0, react_1.useCallback)(function (room_sid, rules) { return __awaiter(_this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            return [2 /*return*/, fetch('/recordingrules', {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ room_sid: room_sid, rules: rules, passcode: user === null || user === void 0 ? void 0 : user.passcode }),
                    method: 'POST',
                }).then(function (res) { return __awaiter(_this, void 0, void 0, function () {
                    var jsonResponse, error;
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0: return [4 /*yield*/, res.json()];
                            case 1:
                                jsonResponse = _c.sent();
                                if (!res.ok) {
                                    error = new Error(((_a = jsonResponse.error) === null || _a === void 0 ? void 0 : _a.message) || 'There was an error updating recording rules');
                                    error.code = (_b = jsonResponse.error) === null || _b === void 0 ? void 0 : _b.code;
                                    return [2 /*return*/, Promise.reject(error)];
                                }
                                return [2 /*return*/, jsonResponse];
                        }
                    });
                }); })];
        });
    }); }, [user]);
    (0, react_1.useEffect)(function () {
        var passcode = getPasscode();
        if (passcode) {
            verifyPasscode(passcode)
                .then(function (verification) {
                if (verification === null || verification === void 0 ? void 0 : verification.isValid) {
                    setUser({ passcode: passcode });
                    window.sessionStorage.setItem('passcode', passcode);
                    history.replace(window.location.pathname);
                }
            })
                .then(function () { return setIsAuthReady(true); });
        }
        else {
            setIsAuthReady(true);
        }
    }, [history]);
    var signIn = (0, react_1.useCallback)(function (passcode) {
        return verifyPasscode(passcode).then(function (verification) {
            if (verification === null || verification === void 0 ? void 0 : verification.isValid) {
                setUser({ passcode: passcode });
                window.sessionStorage.setItem('passcode', passcode);
            }
            else {
                throw new Error(getErrorMessage(verification === null || verification === void 0 ? void 0 : verification.error));
            }
        });
    }, []);
    var signOut = (0, react_1.useCallback)(function () {
        setUser(null);
        window.sessionStorage.removeItem('passcode');
        return Promise.resolve();
    }, []);
    return { user: user, isAuthReady: isAuthReady, getToken: getToken, signIn: signIn, signOut: signOut, updateRecordingRules: updateRecordingRules };
}
exports.default = usePasscodeAuth;
