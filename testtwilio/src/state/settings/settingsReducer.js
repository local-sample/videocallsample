"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.settingsReducer = exports.inputLabels = exports.initialSettings = void 0;
exports.initialSettings = {
    trackSwitchOffMode: undefined,
    dominantSpeakerPriority: 'standard',
    bandwidthProfileMode: 'collaboration',
    maxAudioBitrate: '16000',
    contentPreferencesMode: 'auto',
    clientTrackSwitchOffControl: 'auto',
};
// This inputLabels object is used by ConnectionOptions.tsx. It is used to populate the id, name, and label props
// of the various input elements. Using a typed object like this (instead of strings) eliminates the possibility
// of there being a typo.
exports.inputLabels = (function () {
    var target = {};
    for (var setting in exports.initialSettings) {
        target[setting] = setting;
    }
    return target;
})();
function settingsReducer(state, action) {
    var _a;
    return __assign(__assign({}, state), (_a = {}, _a[action.name] = action.value === 'default' ? undefined : action.value, _a));
}
exports.settingsReducer = settingsReducer;
