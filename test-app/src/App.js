import logo from './logo.svg';
import './App.css';
import LoginPage from './LoginPage';
import LoginSideBox from './LoginSideBox';
import { Routes, Route } from 'react-router-dom';
import RecordsPage from './RecordsPage';
import RecordsComp from './RecordsContext';

function App() {
  return (

    <div className="App">
      {/* <header className="App-header"> */}
      <Routes>
        <Route element={<LoginSideBox />} path='/' index />
        <Route element={<RecordsComp><RecordsPage /></RecordsComp>} path='/records' />
      </Routes>

      {/* //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a> */}
      {/* </header> */}
    </div>
  );
}

export default App;
