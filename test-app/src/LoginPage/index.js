import { TextField, Grid, Button } from "@mui/material";
import React from "react";
import "./styles.css";

const LoginPage = () => {
    const handleSubmit = () => {

    }

    return <Grid className="loginContainer" container>
        <Grid className="container" xs={12} sm={5} md={4} xl={3}>
            <p className="typo">Please Enter Your Credentials</p>
            <input type="text" className="input" required />
            <input type="password" className="input" required />
            <Button onClick={handleSubmit} className="btn">
                Submit
            </Button>
            {/* <input type="submit" value={"submit"} className="btn" /> */}
            {/* <TextField type="text" />
            <TextField type="text" /> */}
        </Grid>
    </Grid>
}

export default LoginPage;