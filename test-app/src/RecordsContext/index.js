import React, { createContext } from "react";

export const RecordsContext = createContext(null);

const RecordsComp = ({ children }) => {
    const [records, setRecords] = React.useState([]);
    const value = {
        records: records,
        setRecords: setRecords
    }
    return <RecordsContext.Provider value={value}>
        {children}
    </RecordsContext.Provider>
}

export default RecordsComp;