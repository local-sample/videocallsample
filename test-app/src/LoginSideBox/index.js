import { Grid, Button } from "@mui/material";
import React from "react";
import { useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/system";
import "./styles.css";
import ScrollToTopButton from "../ScrollToTopButton";

const LoginSideBox = () => {
    const theme = useTheme();
    const sm = useMediaQuery('(max-width:900px)');
    let globalError = {};
    const [isVisible, setIsVisible] = React.useState(false);
    const scrollContainerRef = React.useRef(null);

    const handleScroll = () => {
        console.log("scrollContainerRef = ", scrollContainerRef.current.scrollTop);
        if (scrollContainerRef.current.scrollTop > 100) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    };

    const scrollToTop = () => {
        scrollContainerRef.current.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };


    React.useEffect(() => {
        const container = scrollContainerRef.current;
        container.addEventListener('scroll', handleScroll);
        return () => {
            container.removeEventListener('scroll', handleScroll);
        };
    }, []);

    function showErrorMsg(index, msg) {
        const form_group = document.getElementsByClassName('form-group')[index]
        form_group.classList.add('error')
        form_group.getElementsByTagName('span')[0].textContent = msg;
        const input_name = form_group.getElementsByTagName('input')[0]?.id;
        if (msg == "") {
            if (globalError?.[input_name]) {
                delete globalError[input_name];
            }
        } else {
            globalError = Object.assign({}, globalError, { [input_name]: msg });
        }
    }

    const userNameValidation = () => {
        const username = document.getElementById("user_name")?.value?.trim() || "";
        console.log("username = ", username);
        let usernameErr = "";
        if (username === "") {
            usernameErr = "* Please Enter Your Name"
            showErrorMsg(0, usernameErr)
        } else if (username.length <= 4) {
            usernameErr = "* Username must be atleast 5 Characters"
            showErrorMsg(0, usernameErr)
        } else if (username.length > 14) {
            usernameErr = "* Username should not exceeds 14 Characters"
            showErrorMsg(0, usernameErr)
        } else {
            usernameErr = "";
            showErrorMsg(0, usernameErr);
            // showSuccessMsg(0)
        }
    }

    const passwordValidation = () => {
        const password = document.getElementById("password")?.value?.trim() || "";
        console.log("password = ", password, "546576 = ", password.length, "fghfg = ", typeof password);
        const sequence = /(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|012|123|234|345|456|567|678)+/ig

        const identical = /^(?!.*(.)\1\1.*).*$/igm

        const commonNames = ["123456", "password", "123456789", "12345678", "12345",
            "111111", "1234567", "sunshine", "qwerty", "iloveyou", "princess", "admin", "welcome",
            "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&amp;*", "charlie",
            "aa123456", "donald", "password1", "qwerty123"
        ];
        let errorspassword = "";
        if (password === "") {
            errorspassword = "* Please Enter Password"
            showErrorMsg(1, errorspassword)
        }
        if (password && password.length < 6) {
            errorspassword = 'Password must be at least 8 characters';
            showErrorMsg(1, errorspassword);
        }

        // if (password && password.length >= 6 && !/^(?=.*[\d#?!@$%^&*-]).{8,}$/i.test(password)) {
        //     errorspassword = 'Must contain at least one numeric or special character ';
        //     showErrorMsg(1, errorspassword);
        // }

        if (password && sequence.test(password) || !identical.test(password)) {
            errorspassword = 'Avoid consecutive sequential and identical characters';
            showErrorMsg(1, errorspassword);
        }

        commonNames.forEach(field => {
            if (password == field) {
                errorspassword = "Password is easily guessable";
                showErrorMsg(1, errorspassword);
            }
        });

        if (errorspassword == "") {
            errorspassword = "";
            showErrorMsg(1, errorspassword);
        }
        console.log("errorpassword = ", errorspassword);
    }

    const handleSubmit = () => {
        userNameValidation();
        passwordValidation();
        console.log(" globalError = ", globalError);
        if (Object.keys(globalError) == 0) {
            const username = document.getElementById("user_name")?.value?.trim() || "";
            const password = document.getElementById("password")?.value?.trim() || "";
            fetch('https://dummyjson.com/auth/login', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({

                    username: username,
                    password: password,
                    expiresInMins: 30, // optional, defaults to 60
                })
            })
                .then(res => res.json())
                .then(response => {
                    console.log("response = ", response);
                    localStorage.setItem("token", response?.token);
                    localStorage.setItem("username", response?.username);
                    localStorage.setItem("name", `${response?.firstName} ${response?.lastName}`);
                    localStorage.setItem("email", response?.email);
                    localStorage.setItem("id", response?.id);
                });
        }
    }

    return <Grid container className="totalContainer">
        {!sm ? <Grid item md={6} className="leftItem">
            <h3>
                Your LOGO here
            </h3>
        </Grid> : null}
        <Grid item md={6} xs={12} className="rightItem" ref={scrollContainerRef}>
            <div className="insideDiv">
                <p className="typo">Please Enter Your Credentials</p>
                <div className="form-group">
                    <input type="text" className="input" required id="user_name" onChange={userNameValidation} />
                    <span></span>
                </div>
                <div className="form-group">
                    <input type="password" className="input" required id="password" onChange={passwordValidation} />
                    <span></span>
                </div>
                <Button onClick={handleSubmit} className="btn">
                    Submit
                </Button>
                <button
                    onClick={scrollToTop}
                    className={`scroll-to-top ${isVisible ? 'visible' : ''}`}
                >
                    ↑
                </button>
            </div>

        </Grid>
        {/* <ScrollToTopButton /> */}
    </Grid >

}

export default LoginSideBox;