import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    records: []
}

const recordReducer = createSlice({
    initialState: initialState,
    name: "records",
    reducers: {
        addData: (state, action) => {
            state.records = [...state.records, ...action.payload.records];
        }
    }
});

export const { actions: recordsActions, reducer: recordsReducer } = recordReducer;

export default recordsReducer;