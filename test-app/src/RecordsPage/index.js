import { Button, Dialog, Grid, IconButton } from "@mui/material";
import React from "react";
import './styles.css';
import { useDispatch, useSelector } from "react-redux";
import CloseIcon from '@mui/icons-material/Close';
import { RecordsContext } from "../RecordsContext";

const RecordsPage = () => {
    // const records = useSelector(state => state?.records?.records) || [];
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = React.useState(false);
    const { records, setRecords } = React.useContext(RecordsContext);
    console.log("records = ", records);

    return <Grid container className="container">
        <Grid>
            <table className="table">
                <thead>
                    <tr>
                        <th className="thead">Name</th>
                        <th className="thead">Age</th>
                        <th className="thead">Email</th>
                        <th className="thead">Gender</th>
                        <th className="thead">Description</th>
                        <th className="thead">Role</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className="tdata"></td>
                        <td className="tdata"></td>
                        <td className="tdata"></td>
                        <td className="tdata"></td>
                        <td className="tdata"></td>
                        <td className="tdata"></td>
                    </tr>
                </tbody>
            </table>
        </Grid>
        <Grid>
            <Button onClick={() => setOpenModal(true)}>Add</Button>
        </Grid>
        <Dialog
            open={openModal}
            maxWidth="md"
        // classes={{
        //     paper: {
        //         minWidth: '45%'
        //     }
        // }}
        >
            <Grid className="dialogContainer">
                <div>
                    <IconButton
                        size="small"
                        onClick={() => setOpenModal(false)}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <form>
                    <div className="formContainer">
                        <label for="name">Name:</label>
                        <input type="text" id="name" name="name" />

                        <label for="age">Age:</label>
                        <input type="number" id="age" name="age" />

                        <label for="email">Email:</label>
                        <input type="email" id="email" name="email" />

                        <p>Please select your gender:</p>
                        <div>
                            <label for="male">Male</label>
                            <input type="radio" id="male" name="gender" />
                            <label for="female">Female</label>
                            <input type="radio" id="female" name="gender" />
                        </div>

                        <label for="description">Description:</label>
                        <textarea id="description" name="description" />

                        <p>Please select your role:</p>
                        <div>
                            <label for="devs">Developer</label>
                            <input type="checkbox" id="devs" name="role" />
                            <label for="systemadmin">Super Admin</label>
                            <input type="checkbox" id="systemadmin" name="role" />
                        </div>
                    </div>
                </form>
            </Grid>
        </Dialog>
    </Grid>
}

export default RecordsPage;