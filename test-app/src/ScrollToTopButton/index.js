// // ScrollToTopButton.js
import React, { useState, useEffect } from 'react';

const ScrollToTopButton = () => {
    const [isVisible, setIsVisible] = React.useState(false);

    const handleScroll = () => {
        if (window.scrollY > 200) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    };

    const scrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };

    React.useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <button
            onClick={scrollToTop}
            style={{
                display: isVisible ? 'block' : 'none',
                position: 'fixed',
                bottom: '20px',
                right: '20px',
                fontSize: '2em',
                background: '#333',
                color: '#fff',
                border: 'none',
                borderRadius: '50%',
                padding: '10px',
                cursor: 'pointer',
            }}
        >
            ↑
        </button>
    );
};

export default ScrollToTopButton;


// ScrollToTopButton.js
// import React, { useState, useEffect, useRef } from 'react';
// import './ScrollToTopButton.css';

// const ScrollToTopButton = () => {
//     const [isVisible, setIsVisible] = useState(false);
//     const scrollContainerRef = useRef(null);

//     const handleScroll = () => {
//         if (scrollContainerRef.current.scrollTop > 200) {
//             setIsVisible(true);
//         } else {
//             setIsVisible(false);
//         }
//     };

//     const scrollToTop = () => {
//         scrollContainerRef.current.scrollTo({
//             top: 0,
//             behavior: 'smooth',
//         });
//     };

//     useEffect(() => {
//         const container = scrollContainerRef.current;
//         container.addEventListener('scroll', handleScroll);
//         return () => {
//             container.removeEventListener('scroll', handleScroll);
//         };
//     }, []);

//     return (
//         <div ref={scrollContainerRef} className="scroll-container">
//             <div style={{ height: '1500px' }}>
//                 Scroll down to see the button.
//             </div>
//             <button
//                 onClick={scrollToTop}
//                 className={`scroll-to-top ${isVisible ? 'visible' : ''}`}
//             >
//                 ↑
//             </button>
//         </div>
//     );
// };

// export default ScrollToTopButton;
