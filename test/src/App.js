import React, { useEffect, useSyncExternalStore } from 'react';
import logo from './logo.svg';
import './App.css';
import { todoStore } from './todoStore';
import Additional from './Additional';
import { useDispatch, useSelector } from 'react-redux';
import { counterActions } from './redux/counter/reducer';

function App() {
  const todos = useSyncExternalStore(todoStore.subscribe, todoStore.getSnapshot);
  console.log("todoStore = ", todoStore, "todos = ", todos);
  const dispatch = useDispatch();

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => {
        dispatch(counterActions?.setTableData(json));
        console.log(json);
      })
  }, [])

  const initialState = {
    count: 0
  }
  console.log("gdf = ", useSelector(state => state?.counter?.count));
  const reducer = (state, action) => {
    console.log("action = ", action, "state.count = ", state.count);
    switch (action.type) {
      case "add":
        return { ...state, count: state.count + 1 };
      case "sub":
        return { ...state, count: state.count - 1 };
      default:
        return state;

    }
  }

  // const [count, dispatch] = React.useReducer(reducer, initialState);


  return (

    <div
      className="App"
      style={{ display: 'flex' }}
    // onClick={() => alert("I'm first container")}
    >
      <header className="App-header" style={{ flex: 1 }}>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>


      </header>
      <div
        style={{ flex: 1 }}
        className='rightContainer'
      // onClick={(e) => {
      // e.preventDefault();
      // alert("I'm second container")
      // }}
      >
        <button onClick={() => {
          // dispatch({ type: 'add' });
          dispatch(counterActions?.add());
        }}>
          add
        </button>
        <button onClick={() => {
          // dispatch({ type: 'add' });
          dispatch(counterActions?.sub());
        }}>
          sub
        </button>

        <Additional />
        <button
          onClick={todoStore?.add}
        >
          Add Todo
        </button>
        <ul>
          {(todos || []).map((todo, i) =>
            <li
              key={i}
            >
              {`${todo?.id}_${todo?.text}`}
            </li>
          )}
        </ul>
      </div>
    </div>

  );
}

export default App;
