import React, { useSyncExternalStore, useId } from "react";
import { todoStore } from "./todoStore";
import { useSelector } from "react-redux";

const Additional = () => {
    const [count, setCount] = React.useState(0);
    const [counting, setCounting] = React.useState(false);
    const timerRef = React.useRef(null);
    const todos = useSyncExternalStore(todoStore.subscribe, todoStore.getSnapshot);
    const id = useId();
    console.log("Additional todostore = ", todoStore, "todos = ", todos, "id = ", id);
    const timerObj = {};
    let timerInterval = null;

    const counter = useSelector(state => state?.counter?.count) || 0;
    const tableData = useSelector(state => state?.counter?.tableData) || [];
    const [color, setColor] = useState("");

    const generateColor = () => {
        setColor(Math.random().toString(16).substring(-6));
    };

    // React.useEffect(() => {

    //     if (counting) {
    //         timerInterval = setInterval(() => {
    //             setCount((prevSeconds) => prevSeconds + 1);
    //         }, 1000);
    //     } else {
    //         clearInterval(timerInterval);
    //         timerInterval = undefined
    //     }
    // }, [counting])

    const startCount = () => {
        if (!timerInterval) {
            timerInterval = setInterval(() => {
                setCount((prevSeconds) => prevSeconds + 1);
            }, 1000);
        } else {
            clearInterval(timerInterval);
            timerInterval = undefined
        }
    }

    const stopCount = () => {
        setCounting(false);
    }

    // const startCount = () => {
    //     timerRef.current = setInterval(() => {
    //         setCount((prevSeconds) => prevSeconds + 1);
    //     }, 1000);
    // }

    // const stopCount = () => {
    //     // if (timerObj?.time) {
    //     //     clearInterval(timerObj.time);
    //     // }
    //     clearInterval(timerRef.current);
    // }

    const showTimer = (sec) => {
        const secs = (sec % 60).toString().padStart(2, 0);
        // const mins = Math.floor(sec / 60);
        let hours = Math.floor(sec / (60 * 60));

        let divisor_for_minutes = sec % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);
        // console.log("mins = ", mins, "sec = ", sec);
        return `${hours.toString().padStart(2, 0)}:${minutes.toString().padStart(2, 0)}:${secs}`;
    }



    return (<>
        <p>{`count = ${counter}, color = ${color}`}</p>
        <button onClick={generateColor}>
            generate
        </button>
        <table className='table'>
            <thead>
                <tr>
                    <th className='tdata'>
                        Name
                    </th>
                    <th className='tdata'>
                        User name
                    </th>
                    <th className='tdata'>
                        Email
                    </th>
                    <th className='tdata'>
                        Phone
                    </th>
                    <th className='tdata'>
                        Website
                    </th>
                </tr>
            </thead>
            <tbody>
                {(tableData || []).map((tdata, i) => <tr
                    key={i}
                >
                    <td className='tdata'>
                        {tdata?.name}
                    </td>
                    <td className='tdata'>
                        {tdata?.username}
                    </td>
                    <td className='tdata'>
                        {tdata?.email}
                    </td>
                    <td className='tdata'>
                        {tdata?.phone}
                    </td>
                    <td className='tdata'>
                        {tdata?.website}
                    </td>
                </tr>)}
            </tbody>
        </table>
        <h5>
            Additional
        </h5>
        <button onClick={(e) => {
            // e.stopPropagation();
            alert("I'm third container")
        }}>
            click
        </button>
        <div>{`count = ${count}`}</div>
        {/* {showTimer(count)} */}
        <button
            onClick={startCount}
        >
            start
        </button>
        <button
            onClick={stopCount}
        >
            stop
        </button>
    </>)
}

export default Additional;