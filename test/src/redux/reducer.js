
import { combineReducers } from "redux";

const initialState = {
    count: 0
}

const countreducer = (state = initialState, action) => {
    console.log("action = ", action, "state.count = ", state?.count);
    switch (action.type) {
        case "add":
            return { ...state, count: state?.count + 1 };
        case "sub":
            return { ...state, count: state?.count - 1 };
        default:
            return state;

    }
}

const reducers = combineReducers({
    counter: countreducer
})

export default reducers;