
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    count: 0,
    tableData: []
}

const reducer = createSlice({
    name: "counter",
    initialState,
    reducers: {
        add: (state, action) => {
            state.count = state?.count + 1;
        },
        sub: (state, action) => {
            state.count = state?.count - 1;
        },
        setTableData: (state, action) => {
            console.log("action.payload = ", action?.payload);
            state.tableData = action?.payload;
        }
    }
});

export const { actions: counterActions, reducer: counterReducer } = reducer;

export default counterReducer;
