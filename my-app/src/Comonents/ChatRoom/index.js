import React, { useState, useEffect, useRef } from 'react';
import firebase from 'firebase/app';
import 'firebase/database';
// import "firebase/firestore";


const ChatRoom = ({ roomId, userId }) => {
  const [messages, setMessages] = useState([]);
  const [text, setText] = useState('');
  const [connected, setConnected] = useState(false);
  const database = useRef(null);

  useEffect(() => {
    // Initialize Firebase
    const config = {
      apiKey: "AIzaSyAgr4E1ejuvkuDVs2cXDNes4Z1TR-ZHeto",
      authDomain: "test-ed0c0.firebaseapp.com",
      projectId: "test-ed0c0",
      storageBucket: "test-ed0c0.appspot.com",
      messagingSenderId: "1005910687101",
      appId: "1:1005910687101:web:178d254c84b3b2b70ca014",
      measurementId: "G-97WVJEH7Z3"
    };
    if (!firebase.apps.length) {
    firebase.initializeApp(config);
    }
    database.current = firebase.database().ref(`rooms/${roomId}/messages`);
    // Listen for new messages in the room
    database.current.on('child_added', (snapshot) => {
      const message = snapshot.val();
      // Add the received message to the list of messages
      setMessages((messages) => [...messages, message]);
    });
    setConnected(true);
    return () => {
      // Clean up the Firebase listeners
      database.current.off();
      setConnected(false);
    };
  }, [roomId]);

  const handleTextChange = (event) => {
    setText(event.target.value);
  };

  const handleSendClick = () => {
    // Send the text message to the Firebase database
    const message = { roomId, userId, text, timestamp: Date.now() };
    database.current.push(message);
    // Add the sent message to the list of messages
    setMessages((messages) => [...messages, { ...message, sender: 'me' }]);
    setText('');
  };

  return (
    <div>
      <h1>Room {roomId}</h1>
      <div>
        {messages.map((message, index) => (
          <div key={index}>
            {message.sender === 'me' ? (
              <span style={{ color: 'blue' }}>Me: </span>
            ) : (
              <span style={{ color: 'green' }}>User {message.userId}: </span>
            )}
            {message.text}
          </div>
        ))}
      </div>
      <div>
        <input type="text" value={text} onChange={handleTextChange} />
        <button disabled={!connected} onClick={handleSendClick}>
          Send
        </button>
      </div>
    </div>
  );
};

export default ChatRoom;
