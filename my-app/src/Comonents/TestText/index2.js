// import React from 'react';

// const TestText = ({ originalText, modifiedText }) => {
//   const getDiff = () => {
//     const diff = [];
//     const originalWords = originalText.split(' ');
//     const modifiedWords = modifiedText.split(' ');
//     const maxLength = Math.max(originalWords.length, modifiedWords.length);

//     for (let i = 0; i < maxLength; i++) {
//       const originalWord = originalWords[i] || '';
//       const modifiedWord = modifiedWords[i] || '';

//       if (originalWord === modifiedWord) {
//         diff.push(<span key={i}>{originalWord} </span>);
//       } else {
//         diff.push(
//           <del key={`del-${i}`}>{originalWord.trim()} </del>,
//           <ins key={`ins-${i}`}>{modifiedWord.trim()} </ins>
//         );
//       }
//     }

//     return diff;
//   };

//   return (
//     <div>
//       <h2>Original Text:</h2>
//       <pre>{originalText}</pre>
//       <h2>Modified Text:</h2>
//       <pre>{modifiedText}</pre>
//       <h2>Diff:</h2>
//       <pre>{getDiff()}</pre>
//     </div>
//   );
// };

// export default TestText;




// import React,{useState} from "react";
// // import "./style.css";
// // import { diff_match_patch } from 'diff-match-patch';
// // import 'diff-match-patch-line-and-word'; // import globally to  enhanse the class.
// // import CodeDiff from 'react-text-diff';
// // import ReactDiffViewer, { DiffMethod } from 'react-diff-viewer';
// // import diff from 'simple-text-diff';
// import _ from 'lodash';
// // import { diffString } from 'json-diff';
// // import CodeDiff from 'react-text-diff';
// // import diff from 'diff';
// // const Diff = require('diff');
// import { diffChars } from 'diff';


// export default function TestText() {
// const [currentText, setCurrentText] = useState(''); // current version of the text
//   const [previousText, setPreviousText] = useState(''); // previous version of the text

//   const obj = [
//     {
//     user: 'a',
//     text: "Hi, How are You?"
//   },
//   {
//     user: 'b',
//     text: "hi, Fine."
//   },
//   {
//     user: 'c',
//     text: "Hi, How About You?"
//   },
// ]

//   // Handle click on history button
//   const handleHistoryClick = () => {
//     // Call the text diffing library to calculate the differences between current and previous text
//     const diffResult = diffChars('One diffenec is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Hope you will be happy every day', 'One difference is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Wish you happy');

//     // Render the diff result in the same line of a paragraph
//     let result = '';
//     diffResult.forEach(part => {
//       const { value, added, removed } = part;
//       if (added) {
//         // Render added text with underline
//         result += `<ins>${value}</ins>`;
//       } else if (removed) {
//         // Render removed text with strike-through
//         result += `<s>${value}</s>`;
//       } else {
//         // Render unchanged text
//         result += value;
//       }
//     });

//     // Update the state with the diff result
//     setCurrentText(result);
//   };

//   const handleHistory = (oldText, newText) => {
//     // Call the text diffing library to calculate the differences between current and previous text
//     const diffResult = diffChars(oldText, newText);

//     // Render the diff result in the same line of a paragraph
//     let result = '';
//     diffResult.forEach(part => {
//       const { value, added, removed } = part;
//       if (added) {
//         // Render added text with underline
//         result += `<ins>${value}</ins>`;
//       } else if (removed) {
//         // Render removed text with strike-through
//         result += `<s>${value}</s>`;
//       } else {
//         // Render unchanged text
//         result += value;
//       }
//     });

//     // Update the state with the diff result
//     setCurrentText(result);
//     return result;
//   };

//   const diffAC = () => {
//     let a = obj.find(e => e.user == "a");
//     let c = obj.find(e => e.user == "c");
//     const res = handleHistory(a.text, c.text);
//     return res;
//   }

//   const diffAB = () => {
//     let a = obj.find(e => e.user == "a");
//     let b = obj.find(e => e.user == "b");
//     const res = handleHistory(a.text, b.text);
//     return res;
//   }

//   const diffBC = () => {
//     let b = obj.find(e => e.user == "b");
//     let c = obj.find(e => e.user == "c");
//     const res = handleHistory(b.text, c.text);
//     return res;
//   }

//   const total = () => {
//     const res1 = diffAB();
//     const res2 = diffBC();
//     handleHistory(res1.replace(/<s>|<\/s>|<ins>|<\/ins>/g,""), res2.replace(/<s>|<\/s>|<ins>|<\/ins>/g,""));
//   }

// console.log("currentText = ",currentText);
//   return (
//     <div>
//     <div>
//       {/* Render the current and previous text content */}
//       <div>
//         <h3>Current Text:</h3>
//         <p dangerouslySetInnerHTML={{ __html: 'One difference is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Wish you happy' }} />
//       </div>
//       <div>
//         <h3>Previous Text:</h3>
//         <p>{'One diffenec is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Hope you will be happy every day'}</p>
//       </div>

//       <div>
//       <p dangerouslySetInnerHTML={{ __html: currentText.replace(new RegExp("<s>","g"),"<s style='background-color: red;'>").replace(new RegExp("<ins>","g"),"<ins style='background-color: green; text-decoration: none'>")
//  }} />
//       </div>

//       {/* Render the history button */}
//       <button onClick={handleHistoryClick}>History</button>

//       <button onClick={diffAB}>check User A and User B text</button>
//       <button onClick={diffBC}>check User b and User c text</button>
//       <button onClick={diffAC}>check User A and User c text</button>

      
//     </div>
//     <div> <button onClick={total}>Total Diff</button></div>
//     </div>
//   );
// };

import React,{useState} from 'react';
import { diffChars } from 'diff';

const TestText = () => {
  const strings = ['', `Test message 1234566    
  Rest Api esquiretek`, `Test message 123YTUTUTU  
  Rest Api esquiretek    
  Testing Dev  `];

  const users = [
    {
      text: `Test message 1234566    
      Rest Api esquiretek`,
      color: 'blue',
      name: 'user1'
    },
    {
      text: `Test message 123YTUTUTU  
      Rest Api esquiretek    
      Testing Dev  `,
      color: 'green',
      name: 'user2'
    },
    {
      text: `Test message 123YTUTUTU  
      Rest Api esquiretek    
      Testing Dev  
      rteqw`,
      color: 'grey',
      name: 'user3'
    },
  ]
  const [currentText, setCurrentText] = useState('');
  const addTag = (diff, clr1, clr2) =>{
        let result = '';
        diff.forEach(part => {
      const { value, added, removed } = part;
      if (added) {
        // Render added text with underline
        result += `<ins  style='background-color: ${clr1}; text-decoration: none'>${value}</ins>`;
      } else if (removed) {
        // Render removed text with strike-through
        result += `<s style='background-color: ${clr1}; text-decoration: none'>${value}</s>`;
      } else {
        // Render unchanged text
        result += `<span  style='background-color: ${clr2}; text-decoration: none'>${value}</span>`;
      }
    });
    setCurrentText(result);
    return result;
  }
  // Function to compare and display differences for multiple strings
  const compareStrings = () => {
    const diffResults = [];

    // Loop through each pair of strings and compare them
    for (let i = 0; i < users.length - 1; i++) {
      for (let j = i + 1; j < users.length; j++) {
        const string1 = users[i].text;
        const string2 = users[j].text;

        // Use diffChars function to get the differences between the strings
        const diffs = diffChars(string1, string2);

const addedTag = addTag(diffs, users[j].color, users[i].color);
console.log("diffs = ",diffs,"addedTag = ",addedTag);
        // Push the diffs array to the diffResults array
        diffResults.push({
          version1: string1,
          version2: string2,
          diffs: addedTag
        });
      }
    }

    // Get the differences for all diffResults in a single line
    // let allDifferences = '';
    // let n= 0;
    // diffResults.forEach(diff => {
    //   n = n+1;
    //   allDifferences += `${n}: `;
    //   diff.forEach(d => {
    //     allDifferences += d.value;
    //   });
    //   allDifferences += '\n';
    // });

    // // Display the differences in a single line in the console
    // console.log(allDifferences);
  };

  return (
    <div>
      {/* Render the strings to be compared */}
      {strings.map((string, index) => (
        <div key={index}>{string}</div>
      ))}
      {/* Button to trigger the comparison */}
      <button onClick={compareStrings}>Compare Strings</button>
     <div>
     <p dangerouslySetInnerHTML={{ __html: currentText
      }} />
      </div>

    </div>
  );
};

export default TestText;

// .replace(new RegExp("<s>","g"),"<s style='background-color: red;'>").replace(new RegExp("<ins>","g"),"<ins style='background-color: green; text-decoration: none'>")
// export default function TestText() {
// //   const dmp = new diff_match_patch();
// // const oldText = `He writes the letter.`;
// // const newText = `She wrote the letters.`;
// const oldText = 'One difference is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Wish you happy'
// const newText = 'One diffenec is that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name. In the code below, hovering over interfaced in an editor will show that it returns an Interface, but will show that aliased returns object literal type.Hope you will be happy every day'


// const result1 = diff.diffPatch(oldText, newText);
// console.log("result = ",result1);
// // const diffs = dmp.diff_lineMode(oldText, newText);
// // const html = dmp.diff_prettyHtml(diffs);

// const styleObj = {
//     '& > ins':{
//         backgroundColor: 'green'
//     },
//     color: 'white',
    
//   };

//   return (
//     <div>
//       <h1>Hello StackBlitz!</h1>
//       {/* <h1 dangerouslySetInnerHTML={{ __html: html }} /> */}
//       {/* <CodeDiff oldStr={oldText} newStr={newText} context={100000} outputFormat={'side-by-side'}/>; */}
//       {/* <ReactDiffViewer
//         oldValue={oldText}
//         newValue={newText}
//         compareMethod={DiffMethod.WORDS}
//         splitView={true}
//       /> */}
//       <p dangerouslySetInnerHTML={{ __html: _.replace(result1.before,new RegExp("<del>","g"),"<del style='background-color: red;'>") }}
     
//        />
//       <p dangerouslySetInnerHTML={{ __html: _.replace(result1.after,new RegExp("<ins>","g"),"<ins style='background-color: green;'>") }}
      
//        />

//        {((diffString(oldText, newText)).replace(/(\[32m)/g, "").replace(/(\[31m)/g, "").replace(/(\[39m)/g, "") || '').split(/\n/).map((line, index) => <div key={index}>{line}</div>)}
//       {/* <CodeDiff oldStr={oldText} newStr={newText} context={100000} outputFormat={'side-by-side'}/>; */}
//       {/* <p>Start editing to see some magic happen :)</p> */}
//     </div>
//   );
// }

// const TestText = ({ originalText, modifiedText }) => {
//     const getDiff = () => {
//       const diffResult = Diff.diffChars(originalText, modifiedText);
//       return diffResult.map((part, index) => {
//         const key = `${part.value}-${index}`;
//         if (part.added) {
//           return <ins key={key}>{part.value}</ins>;
//         }
//         if (part.removed) {
//           return <del key={key}>{part.value}</del>;
//         }
//         return <span key={key}>{part.value}</span>;
//       });
//     };
  
//     return (
//       <div>
//         <h2>Original Text:</h2>
//         <pre>{originalText}</pre>
//         <h2>Modified Text:</h2>
//         <pre>{modifiedText}</pre>
//         <h2>Diff:</h2>
//         <pre>{getDiff()}</pre>
//       </div>
//     );
//   };
  
//   export default TestText;
