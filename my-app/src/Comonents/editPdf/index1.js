import React from "react";
import { PDFDocument, StandardFonts, rgb, degrees } from 'pdf-lib';

const createEditablePDF = async () => {
    // Create a new PDF document
    const pdfDoc = await PDFDocument.create();
    // Add a blank page to the document
    const page = pdfDoc.addPage([550, 750]);
    const form = pdfDoc.getForm();
    // Set the font and font size for the form fields
    const font = await pdfDoc.embedFont(StandardFonts.Helvetica);
    const fontSize = 12;

    // Create a text form field
    const textField = form.createTextField('name');
    textField.setText('John Doe');
    textField.enableMultiline(true);
    textField.enableCombing(true);
    // textField.setFont(font);
    // textField.setFontSize(fontSize);
    // textField.setPadding(5);
    // textField.setMaxLength(100);
    // textField.setBorderColor(rgb(0, 0, 0));
    // textField.setBackgroundColor(rgb(1, 1, 1));
    // page.addFormField(textField);
    textField.addToPage(page, {
        x: 50,
        y: 75,
        width: 200,
        height: 100,
        textColor: rgb(1, 0, 0),
        backgroundColor: rgb(0, 1, 0),
        borderColor: rgb(0, 0, 1),
        borderWidth: 2,
        fontSize: fontSize,
        font: font,
    })

    // Create a checkbox form field
    const checkboxField = form.createCheckBox('agree');
    checkboxField.check(true);
    // checkboxField.setAppearanceState('On');
    checkboxField.defaultUpdateAppearances();
    // page.addFormField(checkboxField);
    checkboxField.addToPage(page, {
        x: 100,
        y: 75,
        width: 25,
        height: 25,
        textColor: rgb(1, 0, 0),
        backgroundColor: rgb(0, 1, 0),
        borderColor: rgb(0, 0, 1),
        borderWidth: 2,
    })

    const pdfBytes = await pdfDoc.save();

    // Save the PDF to a file
    const pdfFile = new Blob([pdfBytes], { type: 'application/pdf' });
    // saveAs(pdfFile, 'editable_form.pdf');
    const docUrl = URL.createObjectURL(pdfFile);
    window.open(docUrl, '_blank');
};

const CreateEditPDF = () => {
    return <div>
        <button onClick={createEditablePDF}>download</button>
    </div>
}
export default CreateEditPDF;
