import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    records: [],
    filteredList: [],
    filterItem: "",
    searchItem: "",
    apiLoading: false
}

const listSlice = createSlice({
    name: 'list',
    initialState: initialState,
    reducers: {
        getList: (state, action) => {
            state.apiLoading = true;
        },
        addList: (state, action) => {
            // console.log("addlist = ", action.payload);
            state.records = action.payload.records;
            state.filteredList = action.payload.records;
            state.users = action.payload.users;
            state.apiLoading = false;
        },
        filterSearchList: (state, action) => {
            console.log("filterSearchList = ", action.payload);
            if (!action.payload.searchItem && !action.payload.filterItem) {
                state.filteredList = state.records;
            } else if (action.payload.searchItem && action.payload.filterItem) {
                state.filteredList = (state.records || []).filter((ele) => (ele?.title.includes(action.payload.searchItem) || action.payload.searchItem == ele?.userId) && (ele.userId == action.payload.filterItem));
            }
            else if (action.payload.filterItem) {
                state.filteredList = (state.records || []).filter((ele) => ele.userId == action.payload.filterItem);
            } else if (action.payload.searchItem) {
                state.filteredList = (state.records || []).filter((ele) => ele?.title.includes(action.payload.searchItem) || action.payload.searchItem == ele?.userId);
            }
        },
        setSearchItem: (state, action) => {
            state.searchItem = action.payload.searchItem;
        },
        setFilterItem: (state, action) => {
            state.filterItem = action.payload.filterItem;
        },
    }
});

export const { actions: listActions, reducer: listReducer } = listSlice;