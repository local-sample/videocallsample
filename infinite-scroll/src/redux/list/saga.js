import { takeEvery, all, call, put } from "redux-saga/effects";
import { listActions } from "./reducer";

// Define a function to fetch posts
const fetchPosts = () => fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json());

const fetchUser = () => fetch("https://jsonplaceholder.typicode.com/users").then(response => response.json());

function* listAddSaga() {
    const result = yield call(fetchPosts);
    const users = yield call(fetchUser);
    console.log("result = ", result, "users = ", users);
    yield put(listActions.addList({ records: result, users: users }));
}

function* addListSaga() {
    yield takeEvery("list/getList", listAddSaga);
}

export default function* listSagas() {
    yield all([
        addListSaga()
    ])
}