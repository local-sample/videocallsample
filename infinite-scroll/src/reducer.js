import { createReducer, createSlice } from "@reduxjs/toolkit"

const initialState = {
    count: 0
}

const appReducer = createSlice({
    name: 'counter',
    initialState: initialState,
    reducers: {
        increment: (state, action) => {
            console.log("add enetered");
            state.count = ++state.count;
        },
        decrement: (state, action) => {
            state.count = --state.count;
        }
    }
});

export const { actions: counterActions, reducer: appReducers } = appReducer;

export default appReducers;