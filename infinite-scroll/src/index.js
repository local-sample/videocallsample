import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import appReducers from './reducer';
import { listReducer } from './redux/list/reducer';
import createMiddleWare from "redux-saga";
import listSagas from './redux/list/saga';

const sagaMiddleWare = createMiddleWare();

const rootReducer = {
  // counter: appReducers,
  list: listReducer
}

const store = configureStore({
  reducer: rootReducer,
  middleware: (gDM) => gDM().concat([sagaMiddleWare])
});

sagaMiddleWare.run(listSagas);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
