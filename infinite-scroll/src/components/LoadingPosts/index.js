import './styles.css';

export const LoadingPosts = () => {
    const loadPages = [1, 2, 3, 4, 5, 6, 7, 8];
    return (
        <div className="loadingcontainer">
            {loadPages.map((ele, i) => {
                return <div key={i} className="loadingitem">
                    <span>{`id: `}</span>
                    <span>{`title: `}</span>
                </div>
            })
            }
        </div>
        // <div className="loadingcontainer">
        //     {loadPages.map((ele, i) => {
        //         return <div key={i} className="loadingitem">
        //             <span>{`id: `}</span>
        //             <span>{`title: `}</span>
        //         </div>
        //     })
        //     }
        // </div>
    );
}