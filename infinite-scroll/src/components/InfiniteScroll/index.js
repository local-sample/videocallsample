import React, { useState, useEffect, useRef } from "react"
import { listActions } from "../../redux/list/reducer"
import { useDispatch, useSelector } from "react-redux";
import "./styles.css";
import useFilterSearch from "./hooks/useFilterSearch";
import { LoadingPosts } from "../LoadingPosts";
import useLazyLoading from "./hooks/useLazyLoading";

const InfiniteScroll = (props) => {
    const [filterUser, setFilterUser] = useState("");
    const [searchedText, setSearchedText] = useState("");
    const dispatch = useDispatch();
    const users = useSelector(state => state.list.users) || [];
    const apiLoading = useSelector(state => state.list.apiLoading) || false;
    // const data = useSelector(state => state.list.records) || [];
    const filterData = useFilterSearch();

    const triggerRef = useRef(null);
    const totalPages = Math.ceil(100 / 16) + 1;
    const totalNumPerPage = 16;

    const onGrabData = (currentPage) => {
        // This would be where you'll call your API
        // console.log("currentpage = ", currentPage, "totalPages = ", totalPages, "totalNumperPAge = ", totalNumPerPage, "cond1 = ", ((currentPage - 1) % totalPages) * totalNumPerPage, "cond2 = ",
        //     (totalNumPerPage * (currentPage % totalPages)));
        console.log("cuurentpage = ", currentPage, "totalpages = ", totalPages, "cond = ", totalNumPerPage * (currentPage % totalPages), "pages = ", currentPage % totalPages);
        return new Promise((resolve) => {
            setTimeout(() => {
                const data = filterData.slice(
                    0,
                    totalNumPerPage * (currentPage % totalPages)
                );
                // console.log("inside grabdata = ", data);
                resolve(data);
            }, 3000);
        });
    };

    const { data, loading } = useLazyLoading({ filterData, triggerRef, onGrabData });

    useEffect(() => {
        // const fn = () => {
        //     fetch("https://jsonplaceholder.typicode.com/users").then(response => response.json()).then(res => console.log("res = ", res));
        // }
        dispatch({ type: "list/getList" });
        // fn();
    }, [])

    const handleSearch = (e) => {
        // dispatch(listActions.filterSearchList({ searchItem: e.target.value, filterItem: filterUser }));
        // setSearchedText(e.target.value);

        dispatch(listActions.setSearchItem({ searchItem: e.target.value }));
    }

    const handleSelect = (e) => {
        // dispatch(listActions.filterSearchList({ searchItem: searchedText, filterItem: e.target.value }));
        // setFilterUser(e.target.value);

        dispatch(listActions.setFilterItem({ filterItem: e.target.value }));
    }

    // console.log("data = ", data, "loading = ", loading, "filterData = ", filterData);

    return <div>
        <h1>infinite scroll</h1>
        <div>
            <input type="text" onChange={handleSearch} />
            <select onChange={handleSelect}>
                <option value="">Please select a user</option>
                {(users || []).map((ele, i) => <option key={i} value={ele.id}>{ele.name}</option>)}
            </select>
        </div>
        <div className="container">
            {(data || []).map((ele, i) => {
                return <div key={i} className="item">
                    <span>{`id: ${ele?.userId}`}</span>
                    <span>{`title: ${ele?.title}`}</span>
                </div>
            })}
            {!apiLoading && filterData.length ? <div ref={triggerRef} className="item" style={{
                // visible: loading
            }}>
                <LoadingPosts />
            </div> : null}
        </div>

    </div>
}

export default InfiniteScroll;