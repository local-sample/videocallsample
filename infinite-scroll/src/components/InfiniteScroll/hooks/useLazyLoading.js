import { useReducer, useEffect, useCallback } from "react";
import { debounce } from "lodash";

const intersectionThreshold = 5;
const loadDelay = 500;

const reducer = (state, action) => {
    switch (action.type) {
        case "set":
            return {
                ...state,
                loading: action.payload.loading
            }
        case "getGrabData":
            console.count("test");
            return {
                ...state,
                loading: false,
                data: [...action.payload.data],
                currentPage: state.currentPage + 1
            }
    }
}

const initialState = {
    loading: false,
    data: [],
    currentPage: 1
}

const useLazyLoading = (props) => {
    const { filterData, triggerRef, onGrabData, options } = props;

    const [state, dispatch] = useReducer(reducer, initialState);
    // console.log("satea = ", state, "propss = ", props);
    const _handleEntry = async (entry) => {
        const boundingRect = entry.boundingClientRect;
        const intersectionRect = entry.intersectionRect;
        // console.log("boundingRect = ", boundingRect, "intersectionRect = ", intersectionRect);
        if (!state.loading && entry.isIntersecting && (intersectionRect.bottom - boundingRect.bottom <= intersectionThreshold && filterData.length)) {

            dispatch({ type: "set", payload: { loading: true } });
            const data = await onGrabData(state.currentPage);
            // console.log("data35456 = ", data);
            dispatch({
                type: "getGrabData", payload: {
                    data: data
                }
            })
        }
    }

    const handleEntry = debounce(_handleEntry, loadDelay)

    const onInterSect = useCallback(entries => {
        handleEntry(entries[0]);
    }
        , [handleEntry])

    useEffect(() => {
        if (triggerRef.current) {
            const container = triggerRef.current;
            const observer = new IntersectionObserver(onInterSect, options);
            observer.observe(container);
            return () => {
                observer.disconnect();
            }
        }
    }, [triggerRef, onInterSect, options])

    return state
}

export default useLazyLoading;