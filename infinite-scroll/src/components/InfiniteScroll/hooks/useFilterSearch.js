import { useSelector } from "react-redux"

const useFilterSearch = () => {
    const data = useSelector(state => state.list.records) || [];
    const searchItem = useSelector(state => state.list.searchItem) || "";
    const filterItem = useSelector(state => state.list.filterItem) || "";
    console.log("filterItem = ", filterItem);
    return data.filter((ele) => {
        if (!searchItem && !filterItem) {
            return ele
        } else if (searchItem && filterItem) {
            return (ele?.title.includes(searchItem) || searchItem == ele?.userId) && (ele.userId == filterItem);
        }
        else if (filterItem) {
            return (ele.userId == filterItem);
        } else if (searchItem) {
            return (ele?.title.includes(searchItem) || searchItem == ele?.userId);
        }
    })
}

export default useFilterSearch;