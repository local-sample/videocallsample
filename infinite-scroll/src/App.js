import logo from './logo.svg';
import './App.css';
import { counterActions } from './reducer';
import { useSelector, useDispatch } from 'react-redux';
import Todo from './Todo';
import InfiniteScroll from './components/InfiniteScroll';

function App() {
  const count = useSelector(state => state?.counter?.count) || 0;
  const dispatch = useDispatch();
  return (
    <div className="App">
      <InfiniteScroll />
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div>
        {count}
      </div>
      <div>
        <button onClick={() => dispatch(counterActions?.increment())}>
          increment
        </button>
        <button onClick={() => dispatch(counterActions?.decrement())}>
          decrement
        </button>
      </div> */}
      {/* <Todo /> */}
    </div>
  );
}

export default App;
