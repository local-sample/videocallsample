import React from "react";
import './Todo.css';

const Todo = () => {
    const [openModel, setOpenModel] = React.useState(false);
    const [data, setData] = React.useState([]);
    const [page, setPage] = React.useState(1);
    const totalPerPage = 5;
    const [tableDatas, setTableDatas] = React.useState({});
    const [formStatus, setFormStatus] = React.useState("");

    React.useEffect(() => {
        if (openModel) {
            const form = document.getElementById("createForm");
            if (form) {
                Object.keys(tableDatas).forEach(key => {
                    const input = form.querySelector(`[name="${key}"]`);
                    if (input) {
                        input.value = tableDatas[key];
                    }
                });
            }
        }
    }, [openModel, tableDatas]);

    function showErrorMsg(index, msg) {
        const formGroup = document.getElementsByClassName("formGroup")[index];
        formGroup.getElementsByTagName('span')[0].textContent = msg
    }

    const removeErrorMsg = (index) => {
        const formGroup = document.getElementsByClassName("formGroup")[index];
        formGroup.getElementsByTagName('span')[0].textContent = "";
    }

    const handleUpdate = (e, index) => {
        const row = document.getElementsByName("tableData")[index];
        const rowDatas = row.getElementsByTagName("td");
        console.log("rowDatas = ", rowDatas);
        const tableData = {};
        for (let i of rowDatas) {
            console.log("i = ", i);
            let dataHtml = i.innerHTML;
            let dataName = i.getAttribute("name"); // Correct way to get the attribute value
            console.log("dataHtml = ", dataHtml, "dataName= ", dataName);
        }
        (Array.from(rowDatas) || []).forEach((ele, i) => {
            console.log("ele = ", ele);
            let dataHtml = ele.innerHTML;
            let dataName = ele.getAttribute("name"); // Correct way to get the attribute value
            console.log("dataHtml = ", dataHtml, "dataName= ", dataName);
            if (dataName) {
                tableData[dataName] = dataHtml;
            }
        });
        // setOpenModel(true);
        // const form = document.getElementById("createForm");
        // Object.keys(tableData).forEach(key => {
        //     const input = form.querySelector(`[name="${key}"]`);
        //     if (input) {
        //         input.value = tableData[key];
        //     }
        // });
        setFormStatus(index);
        setOpenModel(true);
        setTableDatas(tableData);
        console.log("tableData = ", tableData);
    }

    const handleSubmit = () => {
        // const formGroup = document.getElementsByClassName("formGroup");
        // console.log("formGroup = ", formGroup);
        // formGroup.forEach((ele, i) => {
        //     showErrorMsg(i, )
        // })
        const createForm = document.getElementById("createForm");
        const form = new FormData(createForm);
        console.log("form = ", Object.entries(form));
        const values = [...form.entries()];
        console.log("values = ", values);
        const obj = {};
        let errorinForm = false;
        values.forEach((e, i) => {
            obj[e[0]] = e[1];
            if (!e[1]) {
                errorinForm = true;
                console.log("e1 = ", e[1]);
                showErrorMsg(i, "required")
            } else {
                removeErrorMsg(i);
            }
        })

        if (!errorinForm) {
            console.log("entered = ", obj);
            if (formStatus == "create") {
                const arr = [];
                arr.push(obj);
                setData([...data, ...arr]);
            } else if (typeof formStatus == 'number') {
                const arr = data;
                arr[formStatus] = obj;
                setData([...arr]);
            }
            createForm.reset();
        }
    }
    console.log("data = ", data);

    const handleOpen = () => {
        setOpenModel(true);
        setTableDatas({});
        setFormStatus("create");
    }

    return <div className="totalDiv">
        <div className="">
            <button className="createBtn" onClick={handleOpen}>Create</button>
        </div>
        <div>
            <table className="datatable">
                <thead>
                    <tr>
                        <th className="th">
                            Name
                        </th>
                        <th className="th">
                            Age
                        </th>
                        <th className="th">
                            Class
                        </th>
                    </tr>
                </thead>

                <tbody>
                    {(data.slice(page * 5 - 5, page * 5) || []).map((ele, i) => <tr key={i} name="tableData">
                        <td className="td" name={"name"}>
                            {ele.name}
                        </td>
                        <td className="td" name="age">
                            {ele.age}
                        </td>
                        <td className="td" name="contactNumber">
                            {ele.contactNumber}
                        </td>
                        <td>
                            <button onClick={(e) => handleUpdate(e, i)}>
                                update
                            </button></td>
                    </tr>)}
                </tbody>
            </table>

            <div>
                <button
                    onClick={() => {
                        if (page != 1) {
                            setPage(page - 1);
                        }
                    }}
                    disabled={page == 1}
                >
                    Left
                </button>
                {[...Array(Math.ceil(data.length / 5))].map((ele, i) => {
                    return <button
                        onClick={() => {
                            setPage(i + 1);
                        }}
                        style={page == (i + 1) ? { backgroundColor: 'lightgrey' } : { backgroundColor: '#fff' }}
                    >
                        {i + 1}
                    </button>
                })
                }
                <button
                    onClick={() => {
                        if (page != (data.slice(page * 5 - 5, page * 5)).length) {
                            setPage(page + 1);
                        }
                    }}
                    disabled={((data).length / 5) < page}
                >
                    Right
                </button>
            </div>
        </div>
        {openModel ? <div className="formDiv">
            <form id="createForm">
                <div className="formGroup">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" />
                    <span></span>
                </div>

                <div className="formGroup">
                    <label for="age">Age</label>
                    <input type="text" id="age" name="age" />
                    <span></span>
                </div>

                <div className="formGroup">
                    <label for="contactNumber">Class</label>
                    <input type="number" name="contactNumber" placeholder="Contact" required id="contactNumber" />
                    <span></span>
                </div>

                {/* <input type="number" name="contactNumber" placeholder="Contact" required /> */}
            </form>
            <button onClick={handleSubmit}>Submit</button>
            <button onClick={() => setOpenModel(false)}>Cancel</button>
        </div> : null}
    </div>
}

export default Todo;