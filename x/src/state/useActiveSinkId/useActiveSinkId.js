"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var constants_1 = require("../../constants");
var useDevices_1 = require("../../hooks/useDevices/useDevices");
function useActiveSinkId() {
    var audioOutputDevices = (0, useDevices_1.default)().audioOutputDevices;
    var _a = (0, react_1.useState)('default'), activeSinkId = _a[0], _setActiveSinkId = _a[1];
    var setActiveSinkId = (0, react_1.useCallback)(function (sinkId) {
        window.localStorage.setItem(constants_1.SELECTED_AUDIO_OUTPUT_KEY, sinkId);
        _setActiveSinkId(sinkId);
    }, []);
    (0, react_1.useEffect)(function () {
        var selectedSinkId = window.localStorage.getItem(constants_1.SELECTED_AUDIO_OUTPUT_KEY);
        var hasSelectedAudioOutputDevice = audioOutputDevices.some(function (device) { return selectedSinkId && device.deviceId === selectedSinkId; });
        if (hasSelectedAudioOutputDevice) {
            _setActiveSinkId(selectedSinkId);
        }
    }, [audioOutputDevices]);
    return [activeSinkId, setActiveSinkId];
}
exports.default = useActiveSinkId;
